package main

import "fmt"

func main() {
	a := 10
	goto skip
	b := 20
skip:
	c := 30
	fmt.Println(a, b, c)
	if c > a {
		goto inner
	}
	if a < b {
	inner:
		fmt.Println("a is less than b")
	}

}
/* --------- output-------------
./prog.go:7:7: goto skip jumps over declaration of b at ./prog.go:8:4
./prog.go:13:8: goto inner jumps into block starting at ./prog.go:15:11

Go build failed.

---------------------------------*/
