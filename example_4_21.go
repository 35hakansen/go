package main

import "fmt"

func main() {
	words := []string{"hi", "salutations", "hello"}
	for _, word := range words {
		switch wordLen := len(word); {
		case wordLen < 5:
			fmt.Println(word, "is a short word!")
		case wordLen > 10:
			fmt.Println(word, "is long word!")
		default:
			fmt.Println(word, "is exactly the right length.")
		}
	}
}


/* -----------OUTPUT------------
hi is a short word!
salutations is long word!
hello is exactly the right length.
-------------------------------*/
